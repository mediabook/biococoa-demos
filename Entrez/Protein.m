//
//  Protein.m
//  Entrez
//
//  Created by Koen van der Drift on 10/1/11.
//  Copyright 2011 The BioCocoa Project. All rights reserved.
//
//  This code is covered by the Creative Commons Share-Alike Attribution license.
//	You are free:
//	to copy, distribute, display, and perform the work
//	to make derivative works
//	to make commercial use of the work
//
//	Under the following conditions:
//	You must attribute the work in the manner specified by the author or licensor.
//	If you alter, transform, or build upon this work, you may distribute the resulting work only under a license identical to this one.
//
//	For any reuse or distribution, you must make clear to others the license terms of this work.
//	Any of these conditions can be waived if you get permission from the copyright holder.
//
//  For more info see: http://creativecommons.org/licenses/by-sa/2.5/

#import "Protein.h"

@implementation Protein

@dynamic accession, info, name, sequence, species, url;

+(Protein *)proteinWithResult:(NSDictionary *)result inManagedObjectContext:(NSManagedObjectContext *)context 
{
 // First try to fetch an existing protein with the accession passed
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Protein" 
                                              inManagedObjectContext:context];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"accession == %@", [result objectForKey: @"accession"]];
    NSFetchRequest *fetch = [[NSFetchRequest alloc] init];
    fetch.entity = entity;
    fetch.predicate = predicate;
    
 // Execute the fetch, and get the resulting protein from the array
    NSArray *proteins = [context executeFetchRequest:fetch error:nil];
    Protein *protein = [proteins lastObject];
    
 // If no existing protein found, create a new one
    if ( nil == protein )
    {
        protein = [NSEntityDescription insertNewObjectForEntityForName:@"Protein" 
                                                inManagedObjectContext:context];
        
        protein.accession = [result objectForKey: @"accession"];
        protein.info = [result objectForKey: @"info"];
        protein.name = [result objectForKey: @"name"];
        protein.sequence = [result objectForKey: @"sequence"];
        protein.species = [result objectForKey: @"species"];
        protein.url = [result objectForKey: @"url"];
    }
    
    [fetch release];
    
    return protein;
}

@end