//
//  Protein.h
//  Entrez
//
//  Created by Koen van der Drift on 10/1/11.
//  Copyright 2011 The BioCocoa Project. All rights reserved.
//
//  This code is covered by the Creative Commons Share-Alike Attribution license.
//	You are free:
//	to copy, distribute, display, and perform the work
//	to make derivative works
//	to make commercial use of the work
//
//	Under the following conditions:
//	You must attribute the work in the manner specified by the author or licensor.
//	If you alter, transform, or build upon this work, you may distribute the resulting work only under a license identical to this one.
//
//	For any reuse or distribution, you must make clear to others the license terms of this work.
//	Any of these conditions can be waived if you get permission from the copyright holder.
//
//  For more info see: http://creativecommons.org/licenses/by-sa/2.5/

#import <Foundation/Foundation.h>

@interface Protein : NSManagedObject

{
}

@property (readwrite, copy) NSString *accession;
@property (readwrite, copy) NSString *info;
@property (readwrite, copy) NSString *name;
@property (readwrite, copy) NSString *sequence;
@property (readwrite, copy) NSString *species;
@property (readwrite, copy) NSString *url;

+(Protein *)proteinWithResult:(NSDictionary *)result inManagedObjectContext:(NSManagedObjectContext *)context;

@end
