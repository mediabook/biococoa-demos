//
//  EntrezAppDelegate.h
//  Entrez
//
//  Created by Koen van der Drift on 10/6/11.
//  Copyright 2011 The BioCocoa Project. All rights reserved.
//
//  This code is covered by the Creative Commons Share-Alike Attribution license.
//	You are free:
//	to copy, distribute, display, and perform the work
//	to make derivative works
//	to make commercial use of the work
//
//	Under the following conditions:
//	You must attribute the work in the manner specified by the author or licensor.
//	If you alter, transform, or build upon this work, you may distribute the resulting work only under a license identical to this one.
//
//	For any reuse or distribution, you must make clear to others the license terms of this work.
//	Any of these conditions can be waived if you get permission from the copyright holder.
//
//  For more info see: http://creativecommons.org/licenses/by-sa/2.5/

#import <Cocoa/Cocoa.h>
#import <BioCocoa/BCFoundation.h>
#import <BioCocoa/BCAppKit.h>


//@class BCSequenceView;

@interface EntrezAppDelegate : NSObject <NSApplicationDelegate> {
    NSWindow *window;
    NSPersistentStoreCoordinator *__persistentStoreCoordinator;
    NSManagedObjectModel *__managedObjectModel;
    NSManagedObjectContext *__managedObjectContext;

    IBOutlet NSArrayController *proteinArrayController;
    IBOutlet BCSequenceView *sequenceView;
    IBOutlet NSTableView *proteinTableView;
}

@property (assign) IBOutlet NSWindow *window;

@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;

- (IBAction)saveAction:(id)sender;
- (IBAction)showDatabaseWindow:(id)sender;
- (IBAction)openInBrowser:(id)sender;

- (void)importFetchedResults:(NSWindow *) searchDatabaseWindow returnCode:(NSInteger)returnCode contextInfo:(void *)contextInfo;

@end
